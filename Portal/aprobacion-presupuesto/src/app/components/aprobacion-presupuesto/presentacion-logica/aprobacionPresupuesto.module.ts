import { NgModule } from '@angular/core';
import { AprobacionPresupuestoRoutingModule } from './aprobacion-presupuesto.routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { CustomMaterialModule } from '../../../core/material-module';
import { AprobacionPresupuestoVentasComponent } from './aprobacion-presupuesto.component';
import { DialogAprobacionPresupuestoComponent } from './dialog-aprobacion-presupuesto.component';
import { AprobacionPresupuestoVentasService } from './aprobacion-presupuesto.service';

@NgModule({
  declarations: [
    AprobacionPresupuestoVentasComponent,
    DialogAprobacionPresupuestoComponent
  ],
  imports: [
    CommonModule,
    CustomMaterialModule,
    FormsModule,
    ReactiveFormsModule,
    AprobacionPresupuestoRoutingModule,
    HttpClientModule
  ],
  providers: [
    AprobacionPresupuestoVentasService
  ],
  bootstrap: [AprobacionPresupuestoVentasComponent],
  exports: [
    AprobacionPresupuestoVentasComponent,
    DialogAprobacionPresupuestoComponent
  ]
})
export class AprobacionPresupuestoModule { }
