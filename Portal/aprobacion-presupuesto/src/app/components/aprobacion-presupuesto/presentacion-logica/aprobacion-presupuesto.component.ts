import { Component, OnDestroy, OnInit } from "@angular/core";
import { AprobacionPresupuestoVentasService } from "./aprobacion-presupuesto.service";
import { TerceroDto } from "../../../core/data/TerceroDto";
import { MatDialog, MatDialogRef } from "@angular/material/dialog";
import { DialogAprobacionPresupuestoComponent } from "./dialog-aprobacion-presupuesto.component";
import { Subscription } from "rxjs";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { Utilitys } from "src/app/core/utilitys";
import { AprobacionPresupuestoVentasDTO } from "src/app/core/data/AprobacionPresupuestoDTO";
import { MatTableDataSource } from "@angular/material/table";
import { element } from "protractor";
import { AuthService } from "ng2-ui-auth";
import { ActivatedRoute } from "@angular/router";
import { JsonPipe } from "@angular/common";
import swal from "sweetalert2";

@Component({
  selector: "actualizador-terceros",
  templateUrl: "../presentacion/aprobacion-presupuesto.component.html",
  styleUrls: ["../presentacion/aprobacion-presupuesto.component.css"],
})
export class AprobacionPresupuestoVentasComponent implements OnInit, OnDestroy {

  dataTable: any = [];
  dataFilter1: any = [];
  tabladatos = false;
  totalPresupuestoC: number = 0;
  totalPresupuestoV: number = 0;
  pdv = false;
  contenedorPantalla = false;
  datasourcefilter: Array<AprobacionPresupuestoVentasDTO> = [];

  dataSource: MatTableDataSource<AprobacionPresupuestoVentasDTO> = new MatTableDataSource(
    [

    ]
  );
  displayedColumns: string[] = [
    "fecha",
    "dia",
    "presupuestoComercial",
    "presupuestoVentas",
    "asignar",
    "mantener",
  ];

  displayedColumns2: string[] = [
    "fecha",
    "dia",
    "presupuestoComercial",
    "presupuestoVentas",
    "asignar",
    "mantener",
  ];

  mes: any;
  anio: any;
  linProducto: any;


  mostrarTabla = false;
  mostrarform = true;

  private dialogConsultar: MatDialogRef<DialogAprobacionPresupuestoComponent>;

  subsTercero: Subscription;

  terceroDataDto: any[];
  caseTwo = "invisible";
  minDate: Date;
  formPresupuesto: FormGroup;
  listaAnos = [];
  listaMeses = [];
  listaProductos = [];

  mesTable: any
  linProdTable: any

  constructor(
    public dialog: MatDialog,
    private aprobacionpresupuestoVentasService: AprobacionPresupuestoVentasService,
    public utilidad: Utilitys
  ) {

    let x = "WnHOVRJN4PT1P1rIOISeXPrH1rODbRww5Hdfeyy0tgQMXvXdY9FVIJAZ3+UM2cWKx5XH9HHH1dlpfV3/FgWUi/immgKz2p0o0v1KA9zn+TcTAJHn7+EDI8mng9GGb7YkFpgcBrS/3omuQCBQuQYHWdov4T0B2BixDYtxBNWvbi/fRaiY+cJeswLw6o/9NHwq"
    // console.log('x', this.utilidad.decrypt(x))



  }


  ngOnInit() {
    this.minDate = new Date(1950, 0, 1);
    this.crearControlesPresupuesto();
    this.consultarListaLineaProductos();

  }


  consultarListaLineaProductos() {
    this.utilidad.abrirLoarder();

    this.aprobacionpresupuestoVentasService.getListaLineaProductos().subscribe(
      (rs: any) => {
        this.utilidad.cerrarLoader();
        if (JSON.parse(this.utilidad.decrypt(rs.data)).estadoSolicitud == "0") {
          let respuesta: any = JSON.parse(
            this.utilidad.decrypt(rs.data)
          ).respuesta;
          this.listaAnos = respuesta.lanios.lanio;
          this.formPresupuesto.controls.anio.setValue(this.listaAnos[0].anio);
          this.listaMeses = respuesta.lmeses.lmeses;
          this.listaProductos = respuesta.lproductos;
        } else {
          this.utilidad.ventanaEmergente(
            "Error",
            "Señor usuario, ocurrio un error al cargar los datos iniciales."
          );
        }
      },
      (error) => {
        this.utilidad.cerrarLoader();
        this.utilidad.ventanaEmergente("Error", "Señor usuario, estamos presentando fallas con el sistema, por favor intente mas tarde nuevamente.")
      }
    );
  }

  crearControlesPresupuesto() {
    this.formPresupuesto = new FormGroup({
      anio: new FormControl("", Validators.required),
      mes: new FormControl("", Validators.required),
      producto: new FormControl("", Validators.required),
    });
  }

  ngOnDestroy() {
    // this.subsTercero.unsubscribe();
  }

  /* datosForm(formPresupuesto: FormGroup) {
     this.aprobacionpresupuestoVentasService.getListaForm().subscribe((rs: any) => {
       this.anios = rs.anios;
       this.meses = rs.meses;
       this.productos = rs.productos;
     });
   }*/

  consultadatatable(formPresupuesto: FormGroup) {

    this.utilidad.abrirLoarder();

    if (this.formPresupuesto.valid && this.validarMeses) {
      this.tabladatos = !false;

      let data = {
        mes: formPresupuesto.value.mes.codMes,
        anio: formPresupuesto.value.anio,
        codLinProducto: formPresupuesto.value.producto.codLinProducto
      }

      this.mesTable = formPresupuesto.value.mes.nombreMes
      this.linProdTable = formPresupuesto.value.producto.nombLinProducto
      this.mes = formPresupuesto.value.mes.codMes;
      this.anio = formPresupuesto.value.anio;
      this.linProducto = formPresupuesto.value.producto.codLinProducto;


      let validacion: any = {


        data: this.utilidad.encrypt(JSON.stringify(data))
      };


      this.aprobacionpresupuestoVentasService.consultarPresupuestoVentas(validacion).subscribe((rs: any) => {

        if (JSON.parse(this.utilidad.decrypt(rs.data)).estadoSolicitud == "0") {
          this.mostrarTabla = true;
          this.mostrarform = false;

          let filtroConsulta: any = this.utilidad.decrypt((rs.data));

          this.utilidad.cerrarLoader()

          let jsonFiltro = JSON.parse(filtroConsulta)
          // console.log('jsonFiltro', jsonFiltro.respuesta)

          this.datasourcefilter = jsonFiltro.respuesta;

          this.dataSource = jsonFiltro.respuesta;
          this.tabladatos = !false;

          //TOTAL PRESUPUESTO COMERCIAL 

          this.totalPresupuestoC = 0;
          this.datasourcefilter.forEach((item: any) => {
            let presupuestoComercial: any = item.presupuestoComercial;
            this.totalPresupuestoC = this.totalPresupuestoC + presupuestoComercial;

          });
          //   console.log('this.totalPresupuestoC', this.totalPresupuestoC)

          //TOTAL PRESUPUESTO VENTAS

          this.totalPresupuestoV = 0;

          this.datasourcefilter.forEach((item: any) => {
            let presupuestoVentas: any = item.presupuestoVentas;
            this.totalPresupuestoV = this.totalPresupuestoV + presupuestoVentas;

          });

          //  console.log('this.totalPresupuestoV', this.totalPresupuestoV)
        } else {
          this.utilidad.ventanaEmergente(
            "Validación",
            "Señor usuario, no se encuentran datos relacionados a esta consulta."
          );
          this.utilidad.focus("#btnAsignar");
          this.utilidad.cerrarLoader();
          this.tabladatos = false;
          this.mostrarTabla = false;

        }

      });
    } else {
      this.utilidad.ventanaEmergente(
        "Validación",
        "Señor usuario, por favor ingrese todos los campos obligatorios."
      );
      this.utilidad.cerrarLoader();
      this.utilidad.focus("#btnAsignar");
    }

  }

  asignarPresupuesto(element: any) {

    let dialogRef = this.dialog.open(DialogAprobacionPresupuestoComponent, {
      width: "400px",
      data: { element },
    });

    dialogRef.afterClosed().subscribe((result) => {


      let presupuesto_ventas = result;

      if (element) {
        //  console.log("element", element);

        let param: any = {
          mes: this.mes,
          anio: this.anio,
          codLinProducto: this.linProducto,
          idTiempo: element.idTiempo,
          valorppto: presupuesto_ventas,
        };

        let validacion: any = {


          data: this.utilidad.encrypt(JSON.stringify(param))
        };

        this.aprobacionpresupuestoVentasService.asignarPresupuestoVentas(validacion)
          .subscribe(
            (rs: any) => {

              let respuesta = JSON.parse(this.utilidad.decrypt(rs.data));


              if (respuesta.estadoSolicitud == "0") {
                this.utilidad.ventanaEmergente(
                  "Exitoso",
                  respuesta.respuesta
                );
                this.utilidad.cerrarLoader();
              } else {
                this.utilidad.ventanaEmergente(
                  "Error",
                  "Señor usuario, ocurrio un error al actualizar los datos."
                );
                this.utilidad.cerrarLoader()
              }

            }, (error) => {
              this.utilidad.cerrarLoader();
              this.utilidad.ventanaEmergente(
                "Error",
                "Señor usuario, estamos presentando fallas con el sistema, por favor intente mas tarde nuevamente."
              );
            });


        setTimeout(() => {
          this.consultadatatable(this.formPresupuesto);
        }, 2000);
      }


    });
  }

  mantenerPresupuesto(element: any) {

    swal({
      title: "Confirmación",
      html: "¿Está seguro de que desea modificar este valor? ",
      confirmButtonText: "Continuar",
      customClass: "estiloModales",
      confirmButtonColor: "#0069c0",
      showCancelButton: true,
    }).then(result => {

      if (result.value == true) {

        this.utilidad.abrirLoarder();

        let presupuesto_ventas = element.presupuestoComercial;

        let param: any = {
          mes: this.mes,
          anio: this.anio,
          codLinProducto: this.linProducto,
          idTiempo: element.idTiempo,
          valorppto: presupuesto_ventas,
        };

        console.log('param', param)

        let validacion = {
          data: this.utilidad.encrypt(JSON.stringify(param))
        };

        this.aprobacionpresupuestoVentasService.asignarPresupuestoVentas(validacion)
          .subscribe(
            (rs: any) => {

              let respuesta: any = JSON.parse(this.utilidad.decrypt(rs.data));


              if (respuesta.estadoSolicitud == "0") {

                this.utilidad.ventanaEmergente(
                  "Exitoso",
                  respuesta.respuesta
                );

                this.utilidad.cerrarLoader();

                setTimeout(() => {
                  this.consultadatatable(this.formPresupuesto);
                }, 2000);
              } else {
                this.utilidad.ventanaEmergente(
                  "Error",
                  "Señor usuario, ocurrio un error al actualizar los datos."
                );
                this.utilidad.cerrarLoader();
              }

            }, (error) => {
              console.log('error', error)
              this.utilidad.cerrarLoader();
              this.utilidad.ventanaEmergente(
                "Error",
                "Señor usuario, estamos presentando fallas con el sistema, por favor intente mas tarde nuevamente."
              );
            });

      } else {
        this.utilidad.ventanaEmergente(
          "Validación",
          "Operación Cancelada"
        )
      }

    });

  }

  cancelar(formPresupuesto: FormGroup) {
    formPresupuesto.reset();
    this.tabladatos = false;
  }

  aprobacionPresupuesto() {

    swal({
      title: "Confirmación",
      html: "¿Está seguro de que desea aprobar este presupuesto? ",
      confirmButtonText: "Continuar",
      customClass: "estiloModales",
      confirmButtonColor: "#0069c0",
      showCancelButton: true,
    }).then(result => {
      console.log('result', result)

      if (result.value == true) {

        this.utilidad.abrirLoarder();

        if (this.totalPresupuestoV == this.totalPresupuestoC) {
          this.pdv = true;
          this.utilidad.cerrarLoader();
        } else {
          this.utilidad.ventanaEmergente(
            "Validación",
            "Señor usuario, Por favor validar que el total del Presupuesto Comercial y Presupuesto Ventas sean iguales."
          );
          this.utilidad.cerrarLoader();
        }
      } else {
        this.utilidad.ventanaEmergente(
          "Validación",
          "Operación Cancelada"
        )
      }





    });

  }

  generarPresupuestoPDV() {

    swal({
      title: "Confirmación",
      html: "¿Está seguro de que desea Generar Presupuesto PDV? ",
      confirmButtonText: "Continuar",
      customClass: "estiloModales",
      confirmButtonColor: "#0069c0",
      showCancelButton: true,
    }).then(result => {

      if (result.value == true) {

        this.utilidad.abrirLoarder();

        let param: any = {

          mes: this.mes,
          anio: this.anio,
          codLinProducto: this.linProducto
        }

        console.log('param', param)

        let validacion: any = {
          data: this.utilidad.encrypt(JSON.stringify(param))
        }

        this.aprobacionpresupuestoVentasService.generarPresupuestoPDV(validacion).
          subscribe(
            (rs: any) => {

              let respuesta : any = JSON.parse(this.utilidad.decrypt(rs.data))
              console.log('respuesta', respuesta)

              if (respuesta.estadoSolicitud == "0") {
                this.utilidad.ventanaEmergente(
                  "Exitoso",
                  respuesta.respuesta
                );

                this.utilidad.cerrarLoader();
              }else{
                this.utilidad.ventanaEmergente(
                  "Error",
                  respuesta.respuesta
                );
                this.utilidad.cerrarLoader();
              }

            }
          )

      } else {
        this.utilidad.ventanaEmergente(
          "Validación",
          "Operación Cancelada"
        )
      }

    })
  }

  validarMeses(formPresupuesto: FormGroup): Boolean {
    let mesNoRepetido = true;
    if (
      formPresupuesto.controls.anio1.value ===
      formPresupuesto.controls.anio2.value ||
      formPresupuesto.controls.anio1.value ===
      formPresupuesto.controls.anio3.value ||
      formPresupuesto.controls.anio2.value ===
      formPresupuesto.controls.anio3.value
    ) {
      if (
        formPresupuesto.controls.mes1.value ===
        formPresupuesto.controls.mes2.value ||
        formPresupuesto.controls.mes1.value ===
        formPresupuesto.controls.mes3.value ||
        formPresupuesto.controls.mes2.value ===
        formPresupuesto.controls.mes3.value
      ) {
        this.utilidad.ventanaEmergente(
          "Validacion",
          "Señor usuario no puede repetir un mes"
        );
        mesNoRepetido = false;
      }
    }

    return mesNoRepetido;
  }

  cancelarConsulta() {
    this.mostrarform = true;
    this.mostrarTabla = false
    this.formPresupuesto.reset();
    let lista: any = [];
    this.dataSource = lista;
    this.pdv = false;
  }
}
