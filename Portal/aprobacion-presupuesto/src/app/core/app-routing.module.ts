import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

const routes: Routes = [
  { path: '', redirectTo: '', pathMatch: 'full' },
  {
    // path: 'financiero/actualizadorTerceros/:data', loadChildren: () =>
    path: 'parametrizacion/aprobacion-presupuesto/:token', loadChildren: () =>
      import('../components/aprobacion-presupuesto/presentacion-logica/aprobacionPresupuesto.module').then(m => m.AprobacionPresupuestoModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
