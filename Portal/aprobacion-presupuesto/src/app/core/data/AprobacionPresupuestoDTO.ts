export interface AprobacionPresupuestoVentasDTO {
  fecha: string,
  nombreDiaSemana?: string,
  presupuestoComercial: Number,
  presupuestoVentas?: Number,
  idTiempo?: Number,
  anio?: string,
  mes?: string,
  producto?: string
}
