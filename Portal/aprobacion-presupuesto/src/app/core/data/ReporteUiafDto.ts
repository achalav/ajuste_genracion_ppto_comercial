export interface ReporteUiafDto {
  fcha ?: Date;
  ttal_trcros_gndres ?: Number;
  ttal_trcros_vlddos ?: Number;
  ttal_trcros_pndientes_por_vldar ?: Number;
  xlsx_bs64 ?: String;
}
