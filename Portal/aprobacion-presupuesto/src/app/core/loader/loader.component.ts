import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-loader',
  template: `<div id="preloader_3"></div>`,
  styleUrls: ['./loader.component.css']
})
export class LoaderComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
