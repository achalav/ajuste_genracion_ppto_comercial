import { BrowserModule } from '@angular/platform-browser';
import { LOCALE_ID, NgModule } from '@angular/core';

import { AppRoutingModule } from './core/app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CustomMaterialModule } from './core/material-module';
import { AppComponent } from './components/inicio/app.component';
import locale from '@angular/common/locales/es-CO';
import { registerLocaleData } from '@angular/common';
import { Utilitys } from './core/utilitys';
registerLocaleData(locale);

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    CustomMaterialModule
  ],
  providers: [{ provide: LOCALE_ID, useValue: 'es-CO'}
,Utilitys],
  bootstrap: [AppComponent]
})
export class AppModule { }
