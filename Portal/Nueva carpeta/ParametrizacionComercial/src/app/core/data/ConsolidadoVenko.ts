export interface ConsolidadoVenko {
  tpo_prcso ?: string;
  fcha_incio_cncliacion ?: Date;
  fcha_fnal_cncliacion ?: Date;
  fcha_incio_prcso ?: Date;
  fcha_fnal_prcso ?: Date;
  cntdad_gndres ?: number;
  cntdad_cncliados ?: number;
}
