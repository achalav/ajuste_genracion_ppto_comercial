import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

const routes: Routes = [
  { path: '', redirectTo: '', pathMatch: 'full' },{
    // path: 'financiero/actualizadorTerceros/:data', loadChildren: () =>
    path: 'parametrizacion/presupuesto/:token', loadChildren: () =>
      import('../components/presupuesto/presentacion-logica/presupuesto.module').then(m => m.PresupuestoModule)
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
