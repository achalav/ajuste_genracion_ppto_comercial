export class Configure {
    private static config: any = null;
    private static status: number;
    public static IpPortZuul: any = null;
    public static IpLogin: any = null;
    public static nitEmpresa: any = null;
    public static tiempoSlider: any = null;

    public static init() {
        this.loadConfigure('../assets/constant/url-config.json');
    }

 
    public static loadConfigure(filePath: string): void {
        const request = new XMLHttpRequest();
        //console.log('request', request);
        request.open('GET', filePath, false);
        request.send();
        if (request.status === 200) {
            this.config = JSON.parse(request.responseText);
        }
        this.status = request.status;
    }



    public static getUrl():string{ 
        this.loadConfigure('../assets/constant/url-config.json');
        return `${this.config.URL}`;
    }

 

    public static getVersion(): string {
        this.loadConfigure('../assets/constant/url-config.json');
        return this.config.VERSION_PORTAL;
    }

 
 


}
