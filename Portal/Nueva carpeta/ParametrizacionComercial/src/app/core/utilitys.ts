import { Injectable } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import Swal from 'sweetalert2';
import { LoaderComponent } from './loader/loader.component';
import * as CryptoJS from 'crypto-js';
import { Router } from '@angular/router';
import { Location } from "@angular/common";

declare var $: any;

@Injectable()
export class Utilitys {

    private dialogRefLoader: MatDialogRef<LoaderComponent>;

    private encryptedKey1 = 'Jk00UlRfMU5LMjBPQ1QxKg==';

    timeOutFocus: any;
    private parsedBase64Key = CryptoJS.enc.Base64.parse(this.encryptedKey1);

    constructor(public dialog: MatDialog,
                 private _location: Location,
                private route: Router) {}
                


    public abrirLoarder = (): void => {
        this.dialogRefLoader = this.dialog.open(LoaderComponent,  { panelClass: 'custom-dialog-container', disableClose: true });
    }

    public cerrarLoader = (): void => {
        this.dialogRefLoader.close();
    }

                /** 
 * @author:Smarthink Consulting Group S.A.S
 * @description: Funcion encargada de hacer focus en determinado componente
 * @param: id me indica el nombre del componente al cual deseo hacerle focus
 **/
  focus(id:any) {
    clearTimeout(this.timeOutFocus);
    this.timeOutFocus = setTimeout(() => {
      if ($(".swal2-shown").length === 0) {
        $(id).focus();
      }
    }, 200);
  }


                  /** 
 * @author:Smarthink Consulting Group S.A.S
 * @description: Funcion encargada de ir a la ruta anterior
 **/
  volverAtras() {
    this._location.back();
  }

    public validarKey = (e: any, pAtrib: string): any => {

        const key = (document.all) ? e.keyCode : e.which;

        if (key === 8) {
            return true;
        }

        let atrib: any;
        if (pAtrib === 'ALF') {
            atrib = /[A-Za-z0-9]/;
        } else if (pAtrib === 'NUM') {
            atrib = /[0-9]/;
        } else if (pAtrib === 'TEXT') {
            atrib = /[A-Za-z ]/;
        }
        return atrib.test(String.fromCharCode(key));
    }


          /** 
 * @author:Smarthink Consulting Group S.A.S
 * @description: Funcion encargada de desplegar ventana emergente
 * @param: titulo titulo de la ventana - tam indica el tamaño de la ventana- indica si es una ventana de confirmacion
 *         mensaje de la ventana emergente
 **/
           ventanaEmergente(titulo:any, mensaje:any, tam?:any, confirmar?:any, focus?:any) {
            $("#bloqPantalla").fadeOut();
        
            if (!tam || tam === "not") {
              tam = "32rem";
            }
        
            if (!confirmar || confirmar === "not") {
              confirmar = "OK";
            }
        
            Swal({
              title: titulo,
              html: mensaje,
              confirmButtonText: confirmar,
              customClass: "estiloModales",
              confirmButtonColor: "#0069c0",
              width: tam
            }).then(result => {
              $("#loader").fadeOut(1000);
              if (focus) {
                setTimeout(() => $(focus).focus(), 200);
              }
              return false;
            });
          }


    public addCeroIzq = (txt: any): string => {

        const longitud = ('' + txt).length;
        let txtTemp = txt;

        for (let index = 0; index < (10 - longitud); index++) {
            txtTemp = '0' + txtTemp;
        }
        return txtTemp;
    }


    public encrypt = (value: string): string => {
      const encryptedData = CryptoJS.AES.encrypt(value, this.parsedBase64Key, {
          mode: CryptoJS.mode.ECB,
          padding: CryptoJS.pad.Pkcs7
      });
      return encryptedData.toString();
    }

    public decrypt = (textToDecrypt: string): string => {
      const decryptedData = CryptoJS.AES.decrypt(textToDecrypt, this.parsedBase64Key, {
          mode: CryptoJS.mode.ECB,
          padding: CryptoJS.pad.Pkcs7
      });
      return decryptedData.toString(CryptoJS.enc.Utf8);
    }

              /** 
 * @author:Smarthink Consulting Group S.A.S
 * @description: Funcion encargada de validarme lo que escribo sobre un input
 * @param: tipo me indica el tipo de validacion que deseo aplicar
 **/
               public validarCaracter(e: any, tipo: string) {
                let tecla = document.all ? e.keyCode : e.which;
                // Tecla de retroceso para borrar, siempre la permite
                if (tecla === 8) {
                  return true;
                }
            
                let patron:any;
                switch (tipo) {
                  case "ALF":
                    patron = /[A-Za-z0-9]/;
                    break;
                  case "NUM":
                    patron = /[0-9]/;
                    break;
                  case "SERV":
                    patron = /[0-9.]/;
                    break;
                  case "SER":
                    patron = /[0-9.-]/;
                    break;
                  case "TEX":
                    patron = /[A-Za-z]/;
                    break;
                  case "DIR": // para direcciones
                    patron = /[A-Za-z0-9#°\- ]/;
                    break;
                  case "RAZ": // para razon social ya que permite punto
                    patron = /[A-Za-z0-9. ]/;
                    break;
                  case "PAS": // se valida que la clave contenga caracteres validos configurados en bd
                    patron = /[A-Za-z0-9\\/:*=-¡!.]/;
                    break;
                  case "ESP": // texto con espacio
                    patron = /[A-Za-z ]/;
                    break;
                  case "DECI":
                    patron = /[0-9.]/;
                    break;
                  case "DECI2":
                    patron = /[0-9.-]/;
                    break;
                }
                return patron.test(String.fromCharCode(tecla));
              }

    public obtenerLabel = (txt): string => {
      txt = txt.slice(1);
      return txt[0].toUpperCase() + txt.slice(1);
    }

    public validarCampoVacio = (txt): string => {
      return (txt === undefined || txt === null) ? '' : txt.toUpperCase();
    }
}
