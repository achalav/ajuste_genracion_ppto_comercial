// Modulos
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CustomMaterialModule } from './../material-module';

// Componentes
import { LoaderComponent } from './loader.component';

@NgModule({
  declarations: [
    LoaderComponent
  ],
  exports: [
    LoaderComponent
  ],
  imports: [
    CommonModule,
    CustomMaterialModule
  ],
  entryComponents: [
    LoaderComponent
  ],
})
export class LoaderModule { }
