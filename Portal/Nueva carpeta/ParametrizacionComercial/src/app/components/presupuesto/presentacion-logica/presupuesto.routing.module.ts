import { AsignarPresupuestoComponent } from './presupuesto.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const ActualizadorTercerosRoutes: Routes = [
  //{ path: 'actualizadorterceros', component: ActualizadorTercerosComponent }
  { path: '', component: AsignarPresupuestoComponent }
];

@NgModule({
  imports: [RouterModule.forChild(ActualizadorTercerosRoutes)],
  exports: [RouterModule]
})
export class PresupuestoRoutingModule { }
