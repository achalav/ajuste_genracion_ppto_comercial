import { Component, OnDestroy, OnInit } from '@angular/core';
import { PresupuestoService } from './presupuesto.service';
import { TerceroDto } from '../../../core/data/TerceroDto';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { DialogAsignarPresupuestoComponent } from './dialog-asignar-presupuesto.component';
import { Subscription } from 'rxjs';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Utilitys } from 'src/app/core/utilitys';

@Component({
  selector: 'actualizador-terceros',
  templateUrl: '../presentacion/presupuesto.component.html',
  styleUrls: ['../presentacion/presupuesto.component.css']
})
export class AsignarPresupuestoComponent implements OnInit, OnDestroy {

  private dialogConsultar: MatDialogRef<DialogAsignarPresupuestoComponent>;

  subsTercero: Subscription;

  terceroDataDto: any[];
  caseTwo = 'invisible';
  minDate: Date;
  formPresupuesto:FormGroup;
  listaAnos =[
    {
      'anio':2022
   
    },{
      'anio':2021
    }
  ];
  listaMeses=[
    {'nombreMes':"enero", 'codMes':1}
  ];
  listaProductos=[];


  constructor(
    public dialog: MatDialog,
    private presupuestoService: PresupuestoService,
    public utilidad: Utilitys
  ) {}

  ngOnInit() {
    this.minDate = new Date(1950, 0, 1);
    this.crearControlesPresupuesto();
    this.consultarListaLineaProductos();
  }

  crearControlesPresupuesto(){
    this.formPresupuesto = new FormGroup({
      anio: new FormControl("", Validators.required),
      mes: new FormControl("", Validators.required),
      anio1: new FormControl("", Validators.required),
      mes1: new FormControl("", Validators.required),
      anio2: new FormControl("", Validators.required),
      mes2: new FormControl("", Validators.required),
      anio3: new FormControl("", Validators.required),
      mes3: new FormControl("", Validators.required),
    })
  }

  ngOnDestroy() {
   // this.subsTercero.unsubscribe();
  }

  consultarListaLineaProductos(){
    this.utilidad.abrirLoarder();
    this.presupuestoService.getListaLineaProductos().subscribe((rs:any)=>{
      this.utilidad.cerrarLoader();
      console.log('rs', JSON.parse(this.utilidad.decrypt(rs.data)));
      if (JSON.parse(this.utilidad.decrypt(rs.data)).estadoSolicitud == "0") {
        let respuesta:any = JSON.parse(this.utilidad.decrypt(rs.data)).respuesta;
        this.listaAnos = respuesta.lanios.lanio;
        console.log('this.listaAnos', this.listaAnos);
        this.formPresupuesto.controls.anio.setValue(this.listaAnos[0].anio);
        this.listaMeses = respuesta.lmeses.lmeses;
        this.listaProductos = respuesta.lproductos;
      } else{
        this.utilidad.ventanaEmergente("Error","Señor usuario, ocurrio un error al cargar los datos iniciales.")
      }
    }, error =>{
      this.utilidad.cerrarLoader();
      this.utilidad.ventanaEmergente("Error","Señor usuario, estamos presentando fallas con el sistema, por favor intente mas tarde nuevamente.")

    })
  }


  cancelar(formPresupuesto:FormGroup){
    formPresupuesto.reset();
  }


  asignar(formPresupuesto:FormGroup) {
    
    if (this.formPresupuesto.valid && this.validarMeses(undefined)) {
     let objetoValidacion = {  
        mes:this.formPresupuesto.controls.mes.value,
        anio:this.formPresupuesto.controls.anio.value
      };
      console.log('objetoValidacion', objetoValidacion);
      let validacion = { data:this.utilidad.encrypt(JSON.stringify(objetoValidacion))};
      console.log('validacion', validacion);
      this.utilidad.abrirLoarder();
      this.presupuestoService.validarPresupuesto(validacion).subscribe((respuestaValidacion:any)=>{
        console.log('respuestaValidacion', respuestaValidacion);
        setTimeout(() => {
          if (JSON.parse(this.utilidad.decrypt(respuestaValidacion.data)).estadoSolicitud == "0") {
            this.utilidad.cerrarLoader();
            this.dialogConsultar = this.dialog.open(DialogAsignarPresupuestoComponent, {
              width: "400px",
              data:{
                productos:this.listaProductos,
                mes:this.formPresupuesto.controls.mes.value,
                anio:this.formPresupuesto.controls.anio.value,
                mes1: this.formPresupuesto.controls.mes1.value,
                anio1: this.formPresupuesto.controls.anio1.value,
                mes2: this.formPresupuesto.controls.mes2.value,
                anio2: this.formPresupuesto.controls.anio2.value,
                mes3: this.formPresupuesto.controls.mes3.value,
                anio3: this.formPresupuesto.controls.anio3.value,
              }
            });
            this.dialogConsultar.afterClosed().subscribe(result => {
              this.utilidad.cerrarLoader();
              formPresupuesto.reset();
              });
          } else {
            this.utilidad.cerrarLoader();
            this.utilidad.ventanaEmergente(
              "Validación","Señor usuario, ya se encuentra asignado un presupuesto para el año : "+ this.formPresupuesto.controls.anio.value +
              " y el mes : "+this.formPresupuesto.controls.mes.value)
          }
        }, 600);

      }, error => {
        console.log('error', error);
        this.utilidad.cerrarLoader();
        this.utilidad.ventanaEmergente("Error","Señor usuario, estamos presentando fallas con el sistema, por favor intente mas tarde nuevamente.")
      })

    } else {
      this.utilidad.ventanaEmergente("Validación","Señor usuario, por favor ingrese todos los campos obligatorios.");
      this.utilidad.focus('#btnAsignar')
    }

  }

  validarMeses(formPresupuesto:FormGroup):Boolean {
    let mesNoRepetido = true;
    if (formPresupuesto.controls.anio1.value === formPresupuesto.controls.anio2.value
      || formPresupuesto.controls.anio1.value === formPresupuesto.controls.anio3.value
      || formPresupuesto.controls.anio2.value === formPresupuesto.controls.anio3.value) {

        if (formPresupuesto.controls.mes1.value === formPresupuesto.controls.mes2.value
          || formPresupuesto.controls.mes1.value === formPresupuesto.controls.mes3.value
          || formPresupuesto.controls.mes2.value === formPresupuesto.controls.mes3.value) {

          this.utilidad.ventanaEmergente("Validacion","Señor usuario no puede repetir un mes");
          mesNoRepetido = false;
        }
    }

    return mesNoRepetido;
  }

}
