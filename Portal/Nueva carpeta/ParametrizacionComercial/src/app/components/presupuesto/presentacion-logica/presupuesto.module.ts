import { AsignarPresupuestoComponent } from './presupuesto.component';
import { NgModule } from '@angular/core';
import { PresupuestoRoutingModule } from './presupuesto.routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PresupuestoService } from './presupuesto.service';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { CustomMaterialModule } from '../../../core/material-module';
import { DialogAsignarPresupuestoComponent } from './dialog-asignar-presupuesto.component';

@NgModule({
  declarations: [
    AsignarPresupuestoComponent,
    DialogAsignarPresupuestoComponent
  ],
  imports: [
    CommonModule,
    CustomMaterialModule,
    FormsModule,
    ReactiveFormsModule,
    PresupuestoRoutingModule,
    HttpClientModule
  ],
  providers: [
    PresupuestoService
  ],
  bootstrap: [AsignarPresupuestoComponent],
  exports: [
    AsignarPresupuestoComponent,
    DialogAsignarPresupuestoComponent
  ]
})
export class PresupuestoModule { }
