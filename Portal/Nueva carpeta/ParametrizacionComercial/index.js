var express = require('express');
var http = require('http');
var path = require('path');

var app = express();

var port = process.env.port || 4203;

app.use(express.static(__dirname + '/dist/ModuloTerceros'));
app.get('/*', (req, res) => res.sendFile(path.join(__dirname)));

const server = http.createServer(app);

server.listen(port, () => console.log('Corriendo...'));

/* var http = require('http');
var express = require('express');
var servidor = express();
//const eurekaHelper = require('./eureka-helper');

var puerto = 3001;

servidor.use(express.static('/dist/ModuloTerceros'));

servidor.get('/', function(request, response){
  console.log('eee');
  response.header('Content-type', 'text/html');
  return response.end();
});

http.createServer(servidor).listen(puerto);

console.log(`Servidor funcionando en http://localhost:${puerto}/`);

//eurekaHelper.registerWithEureka('Express-Angular', puerto); */
