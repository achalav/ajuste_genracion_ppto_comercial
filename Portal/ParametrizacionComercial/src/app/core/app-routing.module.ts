import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

const routes: Routes = [
  { path: '', redirectTo: '', pathMatch: 'full' },{
    // path: 'financiero/actualizadorTerceros/:data', loadChildren: () =>
    path: 'parametrizacion/presupuesto/:token', loadChildren: () =>
      import('../components/presupuesto/presentacion-logica/presupuesto.module').then(m => m.PresupuestoModule)
  },
  {
    // path: 'financiero/actualizadorTerceros/:data', loadChildren: () =>
    path: 'parametrizacion/presupuesto-total/:token', loadChildren: () =>
      import('../components/presupuesto-total/presentacion-logica/presupuesto-total.module').then(m => m.PresupuestoTotalModule)
  },
  {
    // path: 'financiero/actualizadorTerceros/:data', loadChildren: () =>
    path: 'parametrizacion/presupuesto-ventas/:token', loadChildren: () =>
      import('../components/presupuesto-ventas/presentacion-logica/presupuestoVentas.module').then(m => m.PresupuestoVentasModule)
  },
  {
    // path: 'financiero/actualizadorTerceros/:data', loadChildren: () =>
    path: 'parametrizacion/aprobacion-presupuesto/:token', loadChildren: () =>
      import('../components/aprobacion-presupuesto/presentacion-logica/aprobacionPresupuesto.module').then(m => m.AprobacionPresupuestoModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
