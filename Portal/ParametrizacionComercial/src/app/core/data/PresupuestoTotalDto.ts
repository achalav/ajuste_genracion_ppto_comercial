export interface PresupuestoTotalDto {
  cod_linea_producto ?: string;
  linea_producto ?: string;
  presupuesto ?: string;
  presupuesto_anterior ?: string;
}
