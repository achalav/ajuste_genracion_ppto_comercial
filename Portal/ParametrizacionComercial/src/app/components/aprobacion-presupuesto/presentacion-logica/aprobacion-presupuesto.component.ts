import { Component, OnDestroy, OnInit } from "@angular/core";
import { AprobacionPresupuestoVentasService } from "./aprobacion-presupuesto.service";
import { TerceroDto } from "../../../core/data/TerceroDto";
import { MatDialog, MatDialogRef } from "@angular/material/dialog";
import { DialogAprobacionPresupuestoComponent } from "./dialog-aprobacion-presupuesto.component";
import { Subscription } from "rxjs";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { Utilitys } from "src/app/core/utilitys";
import { PresupuestoVentasDTO } from "src/app/core/data/PresupuestoVentasDTO";
import { MatTableDataSource } from "@angular/material/table";
import { element } from "protractor";
import { AuthService } from "ng2-ui-auth";
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: "actualizador-terceros",
  templateUrl: "../presentacion/aprobacion-presupuesto.component.html",
  styleUrls: ["../presentacion/aprobacion-presupuesto.component.css"],
})
export class AprobacionPresupuestoVentasComponent implements OnInit, OnDestroy {
  anios: any = [];
  meses: any = [];
  productos: any = [];
  dataTable: any = [];
  dataFilter1: any = [];
  tabladatos = false;
  totalPresupuestoC: number = 0;
  totalPresupuestoV: number = 0;
  pdv = false;
  contenedorPantalla = false;
  datasourcefilter: Array<PresupuestoVentasDTO> = [];

  dataSource: MatTableDataSource<PresupuestoVentasDTO> = new MatTableDataSource(
    []
  );
  displayedColumns: string[] = [
    "fecha",
    "dia",
    "presupuestoComercial",
    "presupuestoVentas",
    "asignar",
    "mantener",
  ];

  private dialogConsultar: MatDialogRef<DialogAprobacionPresupuestoComponent>;

  subsTercero: Subscription;

  terceroDataDto: any[];
  caseTwo = "invisible";
  minDate: Date;
  formPresupuesto: FormGroup;
  listaAnos = [];
  listaMeses = [];
  listaProductos = [];

  constructor(
    public dialog: MatDialog,
    private aprobacionpresupuestoVentasService: AprobacionPresupuestoVentasService,
    public utilidad: Utilitys
  ) {
    this.dataSource.data = this.dataTable;
    this.datosForm(undefined);
  }

  ngOnInit() {
    this.minDate = new Date(1950, 0, 1);
    this.crearControlesPresupuesto();
    this.consultarListaLineaProductos();
  }

  crearControlesPresupuesto() {
    this.formPresupuesto = new FormGroup({
      anio: new FormControl("", Validators.required),
      mes: new FormControl("", Validators.required),
      producto: new FormControl("", Validators.required),
    });
  }

  ngOnDestroy() {
    // this.subsTercero.unsubscribe();
  }

  datosForm(formPresupuesto: FormGroup) {
    this.aprobacionpresupuestoVentasService.getListaForm().subscribe((rs: any) => {
      this.anios = rs.anios;
      this.meses = rs.meses;
      this.productos = rs.productos;
    });
  }

  consultadatatable(formPresupuesto: FormGroup) {
    if (this.formPresupuesto.valid && this.validarMeses) {
      console.log("formPresupuesto", formPresupuesto.value);
      let anio = formPresupuesto.value.anio;
      let mes = formPresupuesto.value.mes;
      let producto = formPresupuesto.value.producto;

      this.aprobacionpresupuestoVentasService.getListaDataTable().subscribe((rs: any) => {
        this.datasourcefilter = rs;
        console.log("this.datasourcefilter", this.datasourcefilter);

    /*    let filtroConsulta = this.datasourcefilter.filter(
          (filtro) => filtro.anio == anio
        );*/

        /* filtro.mes == mes &&
          filtro.producto == producto && */
       // console.log("filtroConsulta", filtroConsulta);

        this.dataSource = rs;
        this.tabladatos = !false;



 //TOTAL PRESUPUESTO COMERCIAL
 this.totalPresupuestoC=0;

 this.datasourcefilter.forEach((item) => {
       let presupuestoComercial: any =item.presupuesto_comercial;
   this.totalPresupuestoC =  this.totalPresupuestoC +presupuestoComercial;

 });
 console.log('this.totalPresupuestoC', this.totalPresupuestoC)

//TOTAL PRESUPUESTO VENTAS

this.totalPresupuestoV=0;

this.datasourcefilter.forEach((item) => {
   let presupuestoVentas: any =item.presupuestp_ventas;
   console.log('item.presupuestp_ventas', item.presupuestp_ventas)

this.totalPresupuestoV =  this.totalPresupuestoV +presupuestoVentas;

});
console.log('this.totalPresupuestoV', this.totalPresupuestoV)


      });
    } else {
      this.utilidad.ventanaEmergente(
        "Validación",
        "Señor usuario, por favor ingrese todos los campos obligatorios."
      );
      this.utilidad.focus("#btnAsignar");
    }

  }

  consultarListaLineaProductos() {
    //this.utilidad.abrirLoarder();
    this.aprobacionpresupuestoVentasService.getListaLineaProductos().subscribe(
      (rs: any) => {
        this.utilidad.cerrarLoader();
        console.log("rs", JSON.parse(this.utilidad.decrypt(rs.data)));
        if (JSON.parse(this.utilidad.decrypt(rs.data)).estadoSolicitud == "0") {
          let respuesta: any = JSON.parse(
            this.utilidad.decrypt(rs.data)
          ).respuesta;
          this.listaAnos = respuesta.lanios.lanio;
          console.log("this.listaAnos", this.listaAnos);
          this.formPresupuesto.controls.anio.setValue(this.listaAnos[0].anio);
          this.listaMeses = respuesta.lmeses.lmeses;
          this.listaProductos = respuesta.lproductos;
        } else {
          this.utilidad.ventanaEmergente(
            "Error",
            "Señor usuario, ocurrio un error al cargar los datos iniciales."
          );
        }
      },
      (error) => {
        //     this.utilidad.cerrarLoader();
        //    this.utilidad.ventanaEmergente("Error","Señor usuario, estamos presentando fallas con el sistema, por favor intente mas tarde nuevamente.")
      }
    );
  }

  cancelar(formPresupuesto: FormGroup) {
    formPresupuesto.reset();
    this.tabladatos = false;
  }



  aprobacionPresupuesto () {

if (this.totalPresupuestoV == this.totalPresupuestoC) {
  this.pdv=true;
}

  }

  generarPresupuestoPDV () {

  }

  validarMeses(formPresupuesto: FormGroup): Boolean {
    let mesNoRepetido = true;
    if (
      formPresupuesto.controls.anio1.value ===
        formPresupuesto.controls.anio2.value ||
      formPresupuesto.controls.anio1.value ===
        formPresupuesto.controls.anio3.value ||
      formPresupuesto.controls.anio2.value ===
        formPresupuesto.controls.anio3.value
    ) {
      if (
        formPresupuesto.controls.mes1.value ===
          formPresupuesto.controls.mes2.value ||
        formPresupuesto.controls.mes1.value ===
          formPresupuesto.controls.mes3.value ||
        formPresupuesto.controls.mes2.value ===
          formPresupuesto.controls.mes3.value
      ) {
        this.utilidad.ventanaEmergente(
          "Validacion",
          "Señor usuario no puede repetir un mes"
        );
        mesNoRepetido = false;
      }
    }

    return mesNoRepetido;
  }

  asignarPresupuesto(element: any) {

    let dialogRef = this.dialog.open(DialogAprobacionPresupuestoComponent, {
      width: "400px",
      data: { element },
    });

    dialogRef.afterClosed().subscribe((result) => {
      console.log("result", result);

      let presupuesto_ventas = result;

      if (element) {
        console.log("element", element);

        let param: any = {
          dia: null,
          fecha: element.fecha,
          id: element.id,
          presupuesto_comercial: element.presupuesto_comercial,
          presupuestp_ventas: presupuesto_ventas,
        };
        this.aprobacionpresupuestoVentasService
          .putAsignarPresupuesto(presupuesto_ventas)
          .subscribe();
        this.utilidad.ventanaEmergente(
          "Exitoso",
          "Señor usuario, los datos se actualizaron correctamente."
        );

        setTimeout(() => {
          this.consultadatatable( this.formPresupuesto);
        }, 2000);
      }
    });
  }

  mantenerPresupuesto(element: any) {
    //  this.dataSource[element.id - 1].presupuestp_ventas = element.presupuesto_comercial;
    // console.log("this.dataSource", this.dataSource[element.id - 1]);
    let presupuesto_ventas = element.presupuesto_comercial;

    let param: any = {
      dia: element.dia,
      fecha: element.fecha,
      id: element.id,
      presupuesto_comercial: element.presupuesto_comercial,
      presupuestp_ventas: presupuesto_ventas,
    };
    this.aprobacionpresupuestoVentasService.putAsignarPresupuesto(param).subscribe();
    this.utilidad.ventanaEmergente(
      "Exitoso",
      "Señor usuario, los datos se actualizaron correctamente."
    );

    setTimeout(() => {
      this.consultadatatable( this.formPresupuesto);
    }, 2000);
  }
}
