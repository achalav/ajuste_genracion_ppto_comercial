import { Component, OnInit, Inject } from '@angular/core';
import { TerceroDto } from '../../../core/data/TerceroDto';
import { PresupuestoService } from './presupuesto.service';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormArray, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Utilitys } from 'src/app/core/utilitys';

@Component({
  selector: 'app-confirmation-dialog',
  templateUrl: '../presentacion/dialog-asignar-presupuesto.component.html',
  styleUrls: ['../presentacion/presupuesto.component.css']
})
export class DialogAsignarPresupuestoComponent implements OnInit {

  listaProductos=[];
  formaProductos:FormGroup;
  constructor(
    public dialogRef: MatDialogRef<DialogAsignarPresupuestoComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private presupuestoService: PresupuestoService,
    private formProductos:FormBuilder,
    public utilidad: Utilitys
  ) {

    console.log('data', data);
    this.listaProductos=data.productos;
    console.log('this.listaProductos', this.listaProductos);

  }

  ngOnInit() {
    this.crearControlesProductos();
  }

  crearControlesProductos(){
    let jsonString="";
    this.listaProductos.forEach((producto,index) => {
      jsonString += `"${producto.nombLinProducto}":"0",`;
      if (index === this.listaProductos.length - 1) {
      jsonString = `{${jsonString.slice(0, jsonString.length - 1)}}`;
      }
    });
    console.log('jsonString', JSON.parse(jsonString));
    this.formaProductos = this.formProductos.group(JSON.parse(jsonString));
    console.log('this.formProductos', this.formaProductos);

  }

  obtenerListaPresupuestos(){
    let listadoPresupuesto=[];
    console.log('listadoPresupuesto', listadoPresupuesto);
    this.listaProductos.forEach(element => {
      let presupuesto = {
        codLinProducto: element.codLinProducto,
        valorPresupuesto:parseFloat(this.formaProductos.controls[element.nombLinProducto].value)
      }
      listadoPresupuesto.push(presupuesto);
    });
    console.log('listadoPresupuesto', listadoPresupuesto);
    return listadoPresupuesto;
  }


validarValor($event){
  console.log('$event', $event);

}


  asignarPresupuesto() {
    let listadoPresupuestos: any = this.obtenerListaPresupuestos();
    let lista = listadoPresupuestos.filter(presupuesto => {
      return presupuesto.valorPresupuesto === 0
    });
    if (lista.length === 0) {
      if (this.formaProductos.valid) {
        let presupuesto = {
          mes: this.data.mes,
          anio: this.data.anio,
          mes1: this.data.mes1,
          anio1: this.data.anio1,
          mes2: this.data.mes2,
          anio2: this.data.anio2,
          mes3: this.data.mes3,
          anio3: this.data.anio3,
          lpresupuestos: listadoPresupuestos
        }

        console.log('presupuesto', presupuesto);

        let presupuestoData = {
          data: this.utilidad.encrypt(JSON.stringify(presupuesto))
        }

        this.utilidad.abrirLoarder();
        this.presupuestoService.asignarPresupuesto(presupuestoData).subscribe((respuestaPresupuesto: any) => {
          setTimeout(() => {
            if (JSON.parse(this.utilidad.decrypt(respuestaPresupuesto.data)).estadoSolicitud == "0") {
              this.utilidad.cerrarLoader();
              this.utilidad.ventanaEmergente("Registro exitoso", "Se ha asignado exitosamente el presupuesto.");
              this.cancelar();
            } else {
              this.utilidad.cerrarLoader();
              this.utilidad.ventanaEmergente("Error", "Señor usuario, ocurrio un error al asignar el presupuesto.")
            }
          }, 600);
        }, error => {
          this.utilidad.cerrarLoader();
          this.utilidad.ventanaEmergente("Error", "Señor usuario, estamos presentando fallas con el sistema, por favor intente mas tarde nuevamente.")

        })

      } else {
        this.utilidad.ventanaEmergente("Validación", "Señor usuario, por favor ingrese todos los campos obligatorios.")

      }
    } else {
      this.utilidad.ventanaEmergente("Validación", "Señor usuario, el valor de los productos debe ser superior a 0.");
    }

  }

  cancelar() {
    this.dialogRef.close();
  }

}
