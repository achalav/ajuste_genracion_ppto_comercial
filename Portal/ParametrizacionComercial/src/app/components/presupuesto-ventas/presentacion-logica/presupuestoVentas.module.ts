import {  PresupuestoVentasComponent } from './presupuesto-ventas.component';
import { NgModule } from '@angular/core';
import { PresupuestoRoutingModule } from './presupuesto-venta.routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PresupuestoVentasService } from './presupuesto-ventas.service';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { CustomMaterialModule } from '../../../core/material-module';
import { DialogAsignarPresupuestoComponent } from './dialog-asignar-presupuesto.component';
import { AuthService } from 'ng2-ui-auth';

@NgModule({
  declarations: [
    PresupuestoVentasComponent,
    DialogAsignarPresupuestoComponent
  ],
  imports: [
    CommonModule,
    CustomMaterialModule,
    FormsModule,
    ReactiveFormsModule,
    PresupuestoRoutingModule,
    HttpClientModule
  ],
  providers: [
    PresupuestoVentasService, AuthService
  ],
  bootstrap: [PresupuestoVentasComponent],
  exports: [
    PresupuestoVentasComponent,
    DialogAsignarPresupuestoComponent
  ]
})
export class PresupuestoVentasModule { }
