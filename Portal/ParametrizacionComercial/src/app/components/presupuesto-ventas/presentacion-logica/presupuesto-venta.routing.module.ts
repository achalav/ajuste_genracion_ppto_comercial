import { PresupuestoVentasComponent } from './presupuesto-ventas.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const ActualizadorTercerosRoutes: Routes = [
  //{ path: 'actualizadorterceros', component: ActualizadorTercerosComponent }
  { path: '', component: PresupuestoVentasComponent }
];

@NgModule({
  imports: [RouterModule.forChild(ActualizadorTercerosRoutes)],
  exports: [RouterModule]
})
export class PresupuestoRoutingModule { }
