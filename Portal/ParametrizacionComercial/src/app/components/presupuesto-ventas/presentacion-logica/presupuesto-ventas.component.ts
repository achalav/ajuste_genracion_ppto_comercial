import { Component, OnDestroy, OnInit } from "@angular/core";
import { PresupuestoVentasService } from "./presupuesto-ventas.service";
import { TerceroDto } from "../../../core/data/TerceroDto";
import { MatDialog, MatDialogRef } from "@angular/material/dialog";
import { DialogAsignarPresupuestoComponent } from "./dialog-asignar-presupuesto.component";
import { Subscription } from "rxjs";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { Utilitys } from "src/app/core/utilitys";
import { PresupuestoVentasDTO } from "src/app/core/data/PresupuestoVentasDTO";
import { MatTableDataSource } from "@angular/material/table";
import { element } from "protractor";
import { AuthService } from "ng2-ui-auth";
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: "actualizador-terceros",
  templateUrl: "../presentacion/presupuesto-venta.component.html",
  styleUrls: ["../presentacion/presupuesto-venta.component.css"],
})
export class PresupuestoVentasComponent implements OnInit, OnDestroy {
  anios: any = [];
  meses: any = [];
  productos: any = [];
  dataTable: any = [];
  dataFilter1: any = [];
  ocultarBtns = false;
  tabladatos = false;
  contenedorPantalla = false;
  datasourcefilter: Array<PresupuestoVentasDTO> = [];

  dataSource: MatTableDataSource<PresupuestoVentasDTO> = new MatTableDataSource(
    []
  );
  displayedColumns: string[] = [
    "fecha",
    "dia",
    "presupuestoComercial",
    "presupuestoVentas",
    "asignar",
    "mantener",
  ];

  private dialogConsultar: MatDialogRef<DialogAsignarPresupuestoComponent>;

  subsTercero: Subscription;

  terceroDataDto: any[];
  caseTwo = "invisible";
  minDate: Date;
  formPresupuesto: FormGroup;
  listaAnos = [];
  listaMeses = [];
  listaProductos = [];

  constructor(
    public dialog: MatDialog,
    private presupuestoVentasService: PresupuestoVentasService,
    public utilidad: Utilitys
  ) {
    this.dataSource.data = this.dataTable;
    this.datosForm(undefined);
  }

  ngOnInit() {
    this.minDate = new Date(1950, 0, 1);
    this.crearControlesPresupuesto();
    this.consultarListaLineaProductos();
  }

  crearControlesPresupuesto() {
    this.formPresupuesto = new FormGroup({
      anio: new FormControl("", Validators.required),
      mes: new FormControl("", Validators.required),
      producto: new FormControl("", Validators.required),
    });
  }

  ngOnDestroy() {
    // this.subsTercero.unsubscribe();
  }

  datosForm(formPresupuesto: FormGroup) {
    this.presupuestoVentasService.getListaForm().subscribe((rs: any) => {
      this.anios = rs.anios;
      this.meses = rs.meses;
      this.productos = rs.productos;
    });
  }

  consultadatatable(formPresupuesto: FormGroup) {
    console.log("formPresupuesto", formPresupuesto.value);
    let anio = formPresupuesto.value.anio;
    let mes = formPresupuesto.value.mes;
    let producto = formPresupuesto.value.producto;

    this.presupuestoVentasService.getListaDataTable().subscribe((rs: any) => {
      this.datasourcefilter = rs;
      console.log("this.datasourcefilter", this.datasourcefilter);

  /*    let filtroConsulta = this.datasourcefilter.filter(
        (filtro) => filtro.anio == anio
      );*/

      /* filtro.mes == mes &&
        filtro.producto == producto && */
     // console.log("filtroConsulta", filtroConsulta);

      this.dataSource = rs;
      this.tabladatos = !false;
    });
  }

  consultarListaLineaProductos() {
    //this.utilidad.abrirLoarder();
    this.presupuestoVentasService.getListaLineaProductos().subscribe(
      (rs: any) => {
        this.utilidad.cerrarLoader();
        console.log("rs", JSON.parse(this.utilidad.decrypt(rs.data)));
        if (JSON.parse(this.utilidad.decrypt(rs.data)).estadoSolicitud == "0") {
          let respuesta: any = JSON.parse(
            this.utilidad.decrypt(rs.data)
          ).respuesta;
          this.listaAnos = respuesta.lanios.lanio;
          console.log("this.listaAnos", this.listaAnos);
          this.formPresupuesto.controls.anio.setValue(this.listaAnos[0].anio);
          this.listaMeses = respuesta.lmeses.lmeses;
          this.listaProductos = respuesta.lproductos;
        } else {
          this.utilidad.ventanaEmergente(
            "Error",
            "Señor usuario, ocurrio un error al cargar los datos iniciales."
          );
        }
      },
      (error) => {
        //     this.utilidad.cerrarLoader();
        //    this.utilidad.ventanaEmergente("Error","Señor usuario, estamos presentando fallas con el sistema, por favor intente mas tarde nuevamente.")
      }
    );
  }

  cancelar(formPresupuesto: FormGroup) {
    formPresupuesto.reset();
    this.tabladatos = false;
  }

  asignar(formPresupuesto: FormGroup) {
    this.tabladatos = true;
    if (this.formPresupuesto.valid && this.validarMeses) {
      let objetoValidacion = {
        mes: this.formPresupuesto.controls.mes.value,
        anio: this.formPresupuesto.controls.anio.value,
      };
      console.log("objetoValidacion", objetoValidacion);
      let validacion = {
        data: this.utilidad.encrypt(JSON.stringify(objetoValidacion)),
      };
      console.log("validacion", validacion);
      this.utilidad.abrirLoarder();
      this.presupuestoVentasService.validarPresupuesto(validacion).subscribe(
        (respuestaValidacion: any) => {
          console.log("respuestaValidacion", respuestaValidacion);
          setTimeout(() => {
            if (
              JSON.parse(this.utilidad.decrypt(respuestaValidacion.data))
                .estadoSolicitud == "0"
            ) {
              this.utilidad.cerrarLoader();
              this.dialogConsultar = this.dialog.open(
                DialogAsignarPresupuestoComponent,
                {
                  width: "400px",
                  data: {
                    productos: this.listaProductos,
                    mes: this.formPresupuesto.controls.mes.value,
                    anio: this.formPresupuesto.controls.anio.value,
                    mes1: this.formPresupuesto.controls.mes1.value,
                    anio1: this.formPresupuesto.controls.anio1.value,
                    mes2: this.formPresupuesto.controls.mes2.value,
                    anio2: this.formPresupuesto.controls.anio2.value,
                    mes3: this.formPresupuesto.controls.mes3.value,
                    anio3: this.formPresupuesto.controls.anio3.value,
                  },
                }
              );
              this.dialogConsultar.afterClosed().subscribe((result) => {
                this.utilidad.cerrarLoader();
                formPresupuesto.reset();
              });
            } else {
              this.utilidad.cerrarLoader();
              this.utilidad.ventanaEmergente(
                "Validación",
                "Señor usuario, ya se encuentra asignado un presupuesto para el año : " +
                  this.formPresupuesto.controls.anio.value +
                  " y el mes : " +
                  this.formPresupuesto.controls.mes.value
              );
            }
          }, 600);
        },
        (error) => {
          console.log("error", error);
          this.utilidad.cerrarLoader();
          this.utilidad.ventanaEmergente(
            "Error",
            "Señor usuario, estamos presentando fallas con el sistema, por favor intente mas tarde nuevamente."
          );
        }
      );
    } else {
      this.utilidad.ventanaEmergente(
        "Validación",
        "Señor usuario, por favor ingrese todos los campos obligatorios."
      );
      this.utilidad.focus("#btnAsignar");
    }
  }

  validarMeses(formPresupuesto: FormGroup): Boolean {
    let mesNoRepetido = true;
    if (
      formPresupuesto.controls.anio1.value ===
        formPresupuesto.controls.anio2.value ||
      formPresupuesto.controls.anio1.value ===
        formPresupuesto.controls.anio3.value ||
      formPresupuesto.controls.anio2.value ===
        formPresupuesto.controls.anio3.value
    ) {
      if (
        formPresupuesto.controls.mes1.value ===
          formPresupuesto.controls.mes2.value ||
        formPresupuesto.controls.mes1.value ===
          formPresupuesto.controls.mes3.value ||
        formPresupuesto.controls.mes2.value ===
          formPresupuesto.controls.mes3.value
      ) {
        this.utilidad.ventanaEmergente(
          "Validacion",
          "Señor usuario no puede repetir un mes"
        );
        mesNoRepetido = false;
      }
    }

    return mesNoRepetido;
  }

  asignarPresupuesto(element: any) {
    let dialogRef = this.dialog.open(DialogAsignarPresupuestoComponent, {
      width: "400px",
      data: { element },
    });

    dialogRef.afterClosed().subscribe((result) => {
      console.log("result", result);

      let presupuesto_ventas = result;

      if (element) {
        console.log("element", element);

        let param: any = {
          dia: null,
          fecha: element.fecha,
          id: element.id,
          presupuesto_comercial: element.presupuesto_comercial,
          presupuestp_ventas: presupuesto_ventas,
        };
        this.presupuestoVentasService
          .putAsignarPresupuesto(presupuesto_ventas)
          .subscribe();
        this.utilidad.ventanaEmergente(
          "Exitoso",
          "Señor usuario, los datos se actualizaron correctamente."
        );

        setTimeout(() => {
          this.consultadatatable( this.formPresupuesto);
        }, 2000);
      }
    });
  }

  mantenerPresupuesto(element: any) {
    //  this.dataSource[element.id - 1].presupuestp_ventas = element.presupuesto_comercial;
    // console.log("this.dataSource", this.dataSource[element.id - 1]);
    let presupuesto_ventas = element.presupuesto_comercial;

    let param: any = {
      dia: element.dia,
      fecha: element.fecha,
      id: element.id,
      presupuesto_comercial: element.presupuesto_comercial,
      presupuestp_ventas: presupuesto_ventas,
    };
    this.presupuestoVentasService.putAsignarPresupuesto(param).subscribe();
    this.utilidad.ventanaEmergente(
      "Exitoso",
      "Señor usuario, los datos se actualizaron correctamente."
    );

    setTimeout(() => {
      this.consultadatatable( this.formPresupuesto);
    }, 2000);
  }
}
