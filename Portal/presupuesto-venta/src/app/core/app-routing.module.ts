import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

const routes: Routes = [
  { path: '', redirectTo: '', pathMatch: 'full' },
  {
    // path: 'financiero/actualizadorTerceros/:data', loadChildren: () =>
    path: 'parametrizacion/presupuesto-venta/:token', loadChildren: () =>
      import('../components/presupuesto-venta/presentacion-logica/presupuestoVenta.module').then(m => m.PresupuestoVentaModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
