export interface PresupuestoVentasDTO {
  fecha: string,
  dia?: string,
  presupuesto_comercial?: Number,
  presupuestp_ventas?: Number,
  id?: Number,
  anio?: string,
  mes?: string,
  producto?: string
}
