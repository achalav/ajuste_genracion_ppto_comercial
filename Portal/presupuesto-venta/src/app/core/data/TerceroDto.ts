import { GenericDto } from './GenericDto';

export class TerceroDto extends GenericDto {

    dcmnto_antrior?: Number;
    dcmnto?: Number;
    tpo_dcmnto_prvio?: String = '';
    tpo_dcmnto?: String = '';
    prmer_nmbre?: String = '';
    sgndo_nmbre?: String = '';
    prmer_aplldo?: String = '';
    sgndo_aplldo?: String = '';
    tpo_actualzcion?: String;

    constructor(init?: Partial<TerceroDto>) {
        super();
        if (init) {
            Object.assign(this, init);
        }
    }
}
