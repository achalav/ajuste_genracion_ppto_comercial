/*
 * Redcolsa Colombiana De Servicios S.A
 * Copyright 2020-2030 Redcolsa, Todos los derechos reservados
 */

import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { TerceroDto } from '../../../core/data/TerceroDto';
import { Configure } from 'src/app/core/environments/urlConfig';
import { element } from 'protractor';
import { PresupuestoVentasDTO } from 'src/app/core/data/PresupuestoVentasDTO';

/*
 * @author:
 * @description:
 */

@Injectable()
export class PresupuestoVentaService {

  tercero$ = new EventEmitter<TerceroDto>();


  constructor(
    private httpClient: HttpClient
  ) { }

  /**
* @author:Smarthink Consulting Group S.A.S
* @description: Funcion encargada de consultar las politicas de empresa por nit
* @param: data es la informacion necesaria para el servicio
**/
asignarPresupuesto(data) {
  return this.httpClient.post(`${Configure.getUrl()}/presupuesto/registrarMeses/`,
  data)
    .pipe(map(response => response, (error: any) => error));
}

  /**
* @author:Smarthink Consulting Group S.A.S
* @description: Funcion encargada de consultar las politicas de empresa por nit
* @param: data es la informacion necesaria para el servicio
**/
//${Configure.getUrl()}
consultarPresupuestoVentas(data) {
  return this.httpClient.post(`http://localhost:6266/presupuestoVentas/consultarPresupuestoVentas`,
  data)
    .pipe(map(response => response, (error: any) => error));
}

  /**
* @author:Smarthink Consulting Group S.A.S
* @description: Funcion encargada de consultar las politicas de empresa por nit
* @param: data es la informacion necesaria para el servicio
**/
validarPresupuesto(data) {
  return this.httpClient.post(`${Configure.getUrl()}/presupuesto/valid/ar`,
  data)
    .pipe(map(response => response, (error: any) => error));
}


  /**
* @author:Smarthink Consulting Group S.A.S
* @description: Funcion encargada de consultar las politicas de empresa por nit
* @param: data es la informacion necesaria para el servicio
**/
  getListaLineaProductos() {
    return this.httpClient.post(`http://localhost:6266/listadatos/listaLineaProductos/`,null)
      .pipe(map(response => response, (error: any) => error));
  }


  asignarPresupuestoVentas(element:any) {
    console.log('element', element);
    
     return this.httpClient.post(`http://localhost:6266/presupuestoVentas/asignarPresupuestoVentas/`,element
       )
       .pipe(map(response => response, (error: any) => error));
   }

}
