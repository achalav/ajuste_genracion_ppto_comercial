import { Component, OnInit, Inject } from "@angular/core";
import { TerceroDto } from "../../../core/data/TerceroDto";
import { PresupuestoVentaService } from "./presupuesto-venta.service";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import {
  FormArray,
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from "@angular/forms";
import { THIS_EXPR } from "@angular/compiler/src/output/output_ast";
import { Utilitys } from "src/app/core/utilitys";
import { element } from "protractor";
import Swal from 'sweetalert2';

@Component({
  selector: "app-confirmation-dialog",
  templateUrl: "../presentacion/dialog-presupuesto-venta.component.html",
  styleUrls: ["../presentacion/presupuesto-venta.component.css"],
})
export class DialogPresupuestoVentaComponent implements OnInit {
  info: number;

  listaProductos = [];
  formaProductos: FormGroup;
  constructor(
    public dialogRef: MatDialogRef<DialogPresupuestoVentaComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private presupuestoVentaService: PresupuestoVentaService,
    public utilidad: Utilitys
  ) {
    this.crearControles();
    //  console.log("data", data);
    this.listaProductos = data.productos;
    //    console.log('this.listaProductos', this.listaProductos);
  }

  ngOnInit() {
    //  this.crearControlesProductos();
  }

  crearControles() {
    this.formaProductos = new FormGroup({
      valor: new FormControl("", Validators.required),
    })

  }

  obtenerListaPresupuestos() {
    let listadoPresupuesto = [];
    //  console.log("listadoPresupuesto", listadoPresupuesto);
    this.listaProductos.forEach((element) => {
      let presupuesto = {
        codLinProducto: element.codLinProducto,
        valorPresupuesto: parseFloat(
          this.formaProductos.controls[element.nombLinProducto].value
        ),
      };
      listadoPresupuesto.push(presupuesto);
    });
    //console.log("listadoPresupuesto", listadoPresupuesto);
    return listadoPresupuesto;
  }

  validarValor($event) {
    console.log("$event", $event);
  }

  asignar() {

    if (this.formaProductos.valid) {

      Swal({
        title: "Confirmación",
        html: "¿Está seguro de que desea modificar este valor? ",
        confirmButtonText: "Continuar",
        customClass: "estiloModales",
        confirmButtonColor: "#0069c0",
        showCancelButton: true,
      }).then(result => {

        if (result.value == true) {

          this.utilidad.abrirLoarder();

          let monto: any = document.getElementById("valor");
          // console.log("monto", monto.value);
          this.info = parseInt(monto.value);
          console.log('this.info', this.info)


          let data: any = this.info;

          // console.log("data enviada", data);

          this.dialogRef.close(data);
        } else {
          this.utilidad.ventanaEmergente(
            "Validación",
            "Operación Cancelada"
          )
        }

      });

    } else {
      this.utilidad.ventanaEmergente(
        "Validación",
        "Señor usuario, por favor ingrese todos los campos obligatorios."
      );
      this.utilidad.cerrarLoader();
    }




  }

  asignarPresupuesto() {
    let listadoPresupuestos: any = this.obtenerListaPresupuestos();
    let lista = listadoPresupuestos.filter((presupuesto) => {
      return presupuesto.valorPresupuesto === 0;
    });
    if (lista.length === 0) {
      if (this.formaProductos.valid) {
        let presupuesto = {
          mes: this.data.mes,
          anio: this.data.anio,
          mes1: this.data.mes1,
          anio1: this.data.anio1,
          mes2: this.data.mes2,
          anio2: this.data.anio2,
          mes3: this.data.mes3,
          anio3: this.data.anio3,
          lpresupuestos: listadoPresupuestos,
        };

        console.log("presupuesto", presupuesto);

        let presupuestoData = {
          data: this.utilidad.encrypt(JSON.stringify(presupuesto)),
        };

        //    this.utilidad.abrirLoarder();
        this.presupuestoVentaService
          .asignarPresupuesto(presupuestoData)
          .subscribe(
            (respuestaPresupuesto: any) => {
              setTimeout(() => {
                if (
                  JSON.parse(this.utilidad.decrypt(respuestaPresupuesto.data))
                    .estadoSolicitud == "0"
                ) {
                  this.utilidad.cerrarLoader();
                  this.utilidad.ventanaEmergente(
                    "Registro exitoso",
                    "Se ha asignado exitosamente el presupuesto."
                  );
                  this.cancelar();
                } else {
                  this.utilidad.cerrarLoader();
                  this.utilidad.ventanaEmergente(
                    "Error",
                    "Señor usuario, ocurrio un error al asignar el presupuesto."
                  );
                }
              }, 600);
            },
            (error) => {
              //    this.utilidad.cerrarLoader();
              //  this.utilidad.ventanaEmergente("Error", "Señor usuario, estamos presentando fallas con el sistema, por favor intente mas tarde nuevamente.")
            }
          );
      } else {
        this.utilidad.ventanaEmergente(
          "Validación",
          "Señor usuario, por favor ingrese todos los campos obligatorios."
        );
      }
    } else {
      this.utilidad.ventanaEmergente(
        "Validación",
        "Señor usuario, el valor de los productos debe ser superior a 0."
      );
    }
  }

  cancelar() {
    if (this.formaProductos.controls.valor.value != undefined) {
      this.dialogRef.close();
    } else {
      this.dialogRef.close()
    }

  }
}
