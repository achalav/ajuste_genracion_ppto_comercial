import { NgModule } from '@angular/core';
import { PresupuestoVentaRoutingModule } from './presupuesto-venta.routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { CustomMaterialModule } from '../../../core/material-module';
import { PresupuestoVentaComponent } from './presupuesto-venta.component';
import { DialogPresupuestoVentaComponent } from './dialog-presupuesto-venta.component';
import { PresupuestoVentaService } from './presupuesto-venta.service';

@NgModule({
  declarations: [
    PresupuestoVentaComponent,
    DialogPresupuestoVentaComponent
  ],
  imports: [
    CommonModule,
    CustomMaterialModule,
    FormsModule,
    ReactiveFormsModule,
    PresupuestoVentaRoutingModule,
    HttpClientModule
  ],
  providers: [
    PresupuestoVentaService
  ],
  bootstrap: [PresupuestoVentaComponent],
  exports: [
   PresupuestoVentaComponent,
    DialogPresupuestoVentaComponent
  ]
})
export class PresupuestoVentaModule { }
