import { Component, OnDestroy, OnInit } from "@angular/core";
import { PresupuestoService } from "./presupuesto.service";
import { TerceroDto } from "../../../core/data/TerceroDto";
import { MatDialog, MatDialogRef } from "@angular/material/dialog";
import { Subscription } from "rxjs";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { Utilitys } from "src/app/core/utilitys";
import swal from "sweetalert2";

@Component({
  selector: "actualizador-terceros",
  templateUrl: "../presentacion/presupuesto.component.html",
  styleUrls: ["../presentacion/presupuesto.component.css"],
})
export class AsignarPresupuestoComponent implements OnInit, OnDestroy {

  subsTercero: Subscription;

  terceroDataDto: any[];
  caseTwo = "invisible";
  minDate: Date;
  formPresupuesto: FormGroup;
  listaAnos = [];
  listaMeses = [];
  listaProductos = [];

  constructor(
    public dialog: MatDialog,
    private presupuestoService: PresupuestoService,
    public utilidad: Utilitys
  ) {
   
//this.crearControlesPresupuesto();
   // console.log('/listadatos/listaAnios/ ', this.utilidad.decrypt("ddIGDFY2dhKw97e4aOZFy0pbdC7AqA0aCei8Y4N04CGLhAABQaeRE7YfS2KE7zOLLJtw4fK8zg7p/TW33fd8oDu839+lugORm4SXVhFyfxoQaWaKFj4LpnROJVs5UK/uhbsg6oPKYFBzvD3HOhucdk2ZEliwGmdhQxHmdMejvN+t78yjOcSYVd6eH7V2zKe8"))

  }

  ngOnInit() {
    this.minDate = new Date(1950, 0, 1);
    this.crearControlesPresupuesto();
    this.consultarListaLineaProductos();
  }

  crearControlesPresupuesto() {
    this.formPresupuesto = new FormGroup({
      anio: new FormControl("", Validators.required),
      mes: new FormControl("", Validators.required),
      anio1: new FormControl("", Validators.required),
      mes1: new FormControl("", Validators.required),
      anio2: new FormControl("", Validators.required),
      mes2: new FormControl("", Validators.required),
      anio3: new FormControl("", Validators.required),
      mes3: new FormControl("", Validators.required),
    });
  }

  ngOnDestroy() {
    // this.subsTercero.unsubscribe();
  }

  consultarListaLineaProductos() {
       this.utilidad.abrirLoarder();
    this.presupuestoService.getListaLineaProductos().subscribe(
      (rs: any) => {
        this.utilidad.cerrarLoader();
        //console.log("rs", JSON.parse(this.utilidad.decrypt(rs.data)));
        if (JSON.parse(this.utilidad.decrypt(rs.data)).estadoSolicitud == "0") {
          let respuesta: any = JSON.parse(
            this.utilidad.decrypt(rs.data)
          ).respuesta;
          this.listaAnos = respuesta.lanios.lanio;
          //  console.log("this.listaAnos", this.listaAnos);
          this.formPresupuesto.controls.anio.setValue(this.listaAnos[0].anio);
          this.listaMeses = respuesta.lmeses.lmeses;
          this.listaProductos = respuesta.lproductos;
        } else {
          this.utilidad.ventanaEmergente(
            "Error",
            "Señor usuario, ocurrio un error al cargar los datos iniciales."
          );
        }
      },
      (error) => {
        // this.utilidad.cerrarLoader();
        // this.utilidad.ventanaEmergente("Error","Señor usuario, estamos presentando fallas con el sistema, por favor intente mas tarde nuevamente.")
      }
    );
  }

  cancelar(formPresupuesto: FormGroup) {
    formPresupuesto.reset();
  }

  asignar(formPresupuesto: FormGroup) {

    swal({
      title: "Confirmación",
      html: "¿Está seguro de que desea modificar este valor? ",
      confirmButtonText: "Continuar",
      customClass: "estiloModales",
      confirmButtonColor: "#0069c0",
      showCancelButton: true,
    }).then(result => {
      if (result.value == true) {


      this.utilidad.abrirLoarder();

    if (this.formPresupuesto.valid) {
      
      if ( this.validarMeses(formPresupuesto)==true) {
        
      let objetoValidacion = {
        mes: this.formPresupuesto.controls.mes.value,
        anio: this.formPresupuesto.controls.anio.value,
        mes1: this.formPresupuesto.controls.mes1.value,
        anio1: this.formPresupuesto.controls.anio1.value,
        mes2: this.formPresupuesto.controls.mes2.value,
        anio2: this.formPresupuesto.controls.anio2.value,
        mes3: this.formPresupuesto.controls.mes3.value,
        anio3: this.formPresupuesto.controls.anio3.value,
      };
     
      let validacion = {
        data: this.utilidad.encrypt(JSON.stringify(objetoValidacion))
      };

     // this.utilidad.abrirLoarder();
      this.presupuestoService.asignarPresupuesto(validacion).subscribe(
        (respuestaValidacion: any) => {

          setTimeout(() => {
            if (
              JSON.parse(this.utilidad.decrypt(respuestaValidacion.data))
                .estadoSolicitud == "0"
            ) {
              this.utilidad.cerrarLoader();
              this.utilidad.ventanaEmergente(
                "Validación",
                "Señor usuario, se asigno un presupuesto para el año : " +
                this.formPresupuesto.controls.anio.value +
                " y el mes : " +
                this.formPresupuesto.controls.mes.value
              );
              formPresupuesto.reset();

            } else {
              this.utilidad.cerrarLoader();
              this.utilidad.ventanaEmergente(
                "Validación",
                "Señor usuario, ya se encuentra asignado un presupuesto para el año : " +
                this.formPresupuesto.controls.anio.value +
                " y el mes : " +
                this.formPresupuesto.controls.mes.value
              );
            }
          }, 300);
        },
        (error) => {
          console.log("error", error);
          this.utilidad.cerrarLoader();
          this.utilidad.ventanaEmergente(
            "Error",
            "Señor usuario, estamos presentando fallas con el sistema, por favor intente mas tarde nuevamente."
          );
        }
      );
      }

    } else {
      this.utilidad.ventanaEmergente(
        "Validación",
        "Señor usuario, por favor ingrese todos los campos obligatorios."
      );
      this.utilidad.focus("#btnAsignar");
    }

  }else {
      this.utilidad.ventanaEmergente(
        "Validación",
        "Operación Cancelada"
      )
    }

  });

  }

  validarMeses(formPresupuesto:FormGroup): Boolean {
  
   /* if (formPresupuesto.controls.anio1.value == formPresupuesto.controls.anio2.value) {
      this.utilidad.ventanaEmergente(
        "Validacion",
        "Señor usuario no puede repetir un mes"
      );

    }*/


    let mesNoRepetido = true;
    if (
      formPresupuesto.controls.anio1.value ===
      
    formPresupuesto.controls.anio2.value ||
    formPresupuesto.controls.anio1.value ===
    formPresupuesto.controls.anio3.value ||
    formPresupuesto.controls.anio2.value ===
    formPresupuesto.controls.anio3.value
    ) {
      if (
       formPresupuesto.controls.mes1.value ===
       formPresupuesto.controls.mes2.value ||
       formPresupuesto.controls.mes1.value ===
       formPresupuesto.controls.mes3.value ||
       formPresupuesto.controls.mes2.value ===
       formPresupuesto.controls.mes3.value
      ) {
        this.utilidad.ventanaEmergente(
          "Validacion",
          "Señor usuario no puede repetir un mes"
        );
        this.utilidad.cerrarLoader();
        mesNoRepetido = false;
      }
    }

    return mesNoRepetido;
  }
}
